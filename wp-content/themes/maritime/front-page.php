<?php get_template_part('includes/header'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-3 text-center">
			<img class="logo-slider" src="<?php echo get_template_directory_uri(); ?>/img/logo-slider-1.png" alt="">
			<img class="logo-slider" src="<?php echo get_template_directory_uri(); ?>/img/logo-slider-2.png" alt="">
			<img class="logo-slider" src="<?php echo get_template_directory_uri(); ?>/img/logo-slider-3.png" alt="">
		</div>
		<div class="col-md-9">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousel" data-slide-to="1"></li>
			    <li data-target="#myCarousel" data-slide-to="2"></li>
			    <li data-target="#myCarousel" data-slide-to="3"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <img src="http://www.samsung.com/id/consumer-images/product/smartphone/2015/SM-G925FZDEXSE/features/SM-G925FZDEXSE-625537-0.jpg" alt="Chania">
			    </div>

			    <div class="item">
			      <img src="http://www.samsung.com/id/consumer-images/product/smartphone/2015/SM-G925FZDEXSE/features/SM-G925FZDEXSE-625537-0.jpg" alt="Chania">
			    </div>

			    <div class="item">
			      <img src="http://www.samsung.com/id/consumer-images/product/smartphone/2015/SM-G925FZDEXSE/features/SM-G925FZDEXSE-625537-0.jpg" alt="Flower">
			    </div>

			    <div class="item">
			      <img src="http://www.samsung.com/id/consumer-images/product/smartphone/2015/SM-G925FZDEXSE/features/SM-G925FZDEXSE-625537-0.jpg" alt="Flower">
			    </div>
			  </div>

			  <!-- Left and right controls -->
			  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-md-12">
			<div id="content" role="main">
				<h1 class="around-line"><span>HOT PRODUCTS</span></h1>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-4 background-product-list">
			<div class="row background-product-list-header">
				<h3>Satelite Phones</h3>
			</div>
			<?php
		        $args = array( 'post_type' => 'product', 'posts_per_page' => 3, 'product_cat' => 'satellite-phones', 'orderby' => 'rand' );
		        $loop = new WP_Query( $args );
		        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
		        <div class="row product-list-item">
					<div class="col-md-4">
					<?php woocommerce_show_product_sale_flash( $post, $product ); ?>
					<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(100,100)); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive"/>'; ?>
					</div>
					<div class="col-md-8">
						<h4 style="margin-top: 0px;"><?php the_title(); ?></h4>
						<p class="price">
							<strong><?php echo $product->get_price_html(); ?></strong>
						</p>
						<p>
							<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="btn btn-primary btn-sm">View Product</a>
							<br/>
							<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
						</p>
					</div>
				</div>

		    <?php endwhile; ?>
		    <?php wp_reset_query(); ?>
			<div class="row product-list-item-more text-right">
				<p>
					<a href="<?php echo get_site_url(); ?>/product-category/satellite-phones" class="btn btn-success">See More Product</a>
				</p>
			</div>
		</div>
		<div class="col-md-4 background-product-list">
			<div class="row background-product-list-header">
				<h3>Prepaid Card</h3>
			</div>
			<?php
		        $args = array( 'post_type' => 'product', 'posts_per_page' => 3, 'product_cat' => 'iridium-prepaid-sim', 'orderby' => 'rand' );
		        $loop = new WP_Query( $args );
		        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
		        <div class="row product-list-item">
					<div class="col-md-4">
					<?php woocommerce_show_product_sale_flash( $post, $product ); ?>
					<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(100,100)); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive"/>'; ?>
					</div>
					<div class="col-md-8">
						<h4 style="margin-top: 0px;"><?php the_title(); ?></h4>
						<p class="price">
							<strong><?php echo $product->get_price_html(); ?></strong>
						</p>
						<p>
							<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="btn btn-primary btn-sm">View Product</a>
							<br/>
							<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
						</p>
					</div>
				</div>

		    <?php endwhile; ?>
		    <?php wp_reset_query(); ?>
			<div class="row product-list-item-more text-right">
				<p>
					<a href="<?php echo get_site_url(); ?>/product-category/iridium-prepaid-sim" class="btn btn-success">See More Product</a>
				</p>
			</div>
		</div>
		<div class="col-md-4 background-product-list">
			<div class="row background-product-list-header">
				<h3>Accecories</h3>
			</div>
			<?php
		        $args = array( 'post_type' => 'product', 'posts_per_page' => 3, 'product_cat' => 'accecories', 'orderby' => 'rand' );
		        $loop = new WP_Query( $args );
		        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
		        <div class="row product-list-item">
					<div class="col-md-4">
					<?php woocommerce_show_product_sale_flash( $post, $product ); ?>
					<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array(100,100)); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive"/>'; ?>
					</div>
					<div class="col-md-8">
						<h4 style="margin-top: 0px;"><?php the_title(); ?></h4>
						<p class="price">
							<strong><?php echo $product->get_price_html(); ?></strong>
						</p>
						<p>
							<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>" class="btn btn-primary btn-sm">View Product</a>
							<br/>
							<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
						</p>
					</div>
				</div>

		    <?php endwhile; ?>
		    <?php wp_reset_query(); ?>
			<div class="row product-list-item-more text-right">
				<p>
					<a href="<?php echo get_site_url(); ?>/product-category/accecories" class="btn btn-success">See More Product</a>
				</p>
			</div>
		</div>
	</div>
</div><!-- /.container -->
<div class="cta-background">
	<div class="container">
		<div class="row">
	      	<div class="col-sm-4">
				<div class="text-center">
					<img class="cta-icon" src="<?php echo get_template_directory_uri(); ?>/img/call.png" alt="">
					<p>Call Us (021 7976391)</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<img class="cta-icon" src="<?php echo get_template_directory_uri(); ?>/img/mail.png" alt="">
					<p>Email Us</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<img class="cta-icon" src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="">
					<p>Chat Us</p>
				</div>
			</div>
	    </div>
    </div>
</div>
<?php get_template_part('includes/footer'); ?>
