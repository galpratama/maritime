<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'maritime');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zj06v,E.VAcx*b2L9%PUkIYrQQ(uO+@8c__=|`pWU/wEl.^+)?=GU&P15MPOMuWc');
define('SECURE_AUTH_KEY',  'AA^y-$lrJ2QW,tH_|)p%_OOHo8d|*WLoI&a]^vqy *i-|Q(<-sha{Ban5(fBv1+O');
define('LOGGED_IN_KEY',    '5*J 8b$L3BS-EgD*EO)Hb+e2m$3H]E+89j-,V7%OBLYl@D(|LI4{5=z_(k%co-K!');
define('NONCE_KEY',        '<4`%}pJ8MemG L1-8s):WL+=[Zh2:<`,wJfB>5]-S=>1zE$9@Bh9x4c-lA6%-L.a');
define('AUTH_SALT',        '9F^:g.#P4P=6yF[^&-{mwTIMB:i6+e*|j*JEPSH.juq~#qSp@0uh|yS{t,=W[U4O');
define('SECURE_AUTH_SALT', ']MGmBsMtT@V.{oF7u>c;=6eqy-}}+=HAn=Egk78a0lG~V%/5hJv@@+D!1q!PAw(/');
define('LOGGED_IN_SALT',   '[^od5NR.GNJhyjO +zx4&|7@J_,G<A1 q</#4_L:>~.Sm9+ENPlsY:6.J;ib -Ci');
define('NONCE_SALT',       'n~OFi;X)Ss(8< 7}a&J+@cUT:a_J)WS;VJj)tiYr~X`vxlnR&G{1/;Bd`G&)7OD.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
