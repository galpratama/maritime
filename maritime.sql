-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2015 at 02:35 PM
-- Server version: 5.6.23
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maritime`
--

-- --------------------------------------------------------

--
-- Table structure for table `mt_booking_block_price_attribute_meta`
--

CREATE TABLE IF NOT EXISTS `mt_booking_block_price_attribute_meta` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `attribute_id` varchar(50) NOT NULL,
  `meta_value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_booking_block_price_meta`
--

CREATE TABLE IF NOT EXISTS `mt_booking_block_price_meta` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `minimum_number_of_days` int(11) NOT NULL,
  `maximum_number_of_days` int(11) NOT NULL,
  `price_per_day` double NOT NULL,
  `fixed_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_booking_fixed_blocks`
--

CREATE TABLE IF NOT EXISTS `mt_booking_fixed_blocks` (
  `id` int(11) NOT NULL,
  `global_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `block_name` varchar(50) NOT NULL,
  `number_of_days` int(11) NOT NULL,
  `start_day` varchar(50) NOT NULL,
  `end_day` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `block_type` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_booking_history`
--

CREATE TABLE IF NOT EXISTS `mt_booking_history` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `weekday` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `from_time` varchar(50) NOT NULL,
  `to_time` varchar(50) NOT NULL,
  `total_booking` int(11) NOT NULL,
  `available_booking` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_booking_order_history`
--

CREATE TABLE IF NOT EXISTS `mt_booking_order_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mt_commentmeta`
--

CREATE TABLE IF NOT EXISTS `mt_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_comments`
--

CREATE TABLE IF NOT EXISTS `mt_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_comments`
--

INSERT INTO `mt_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Tuan WordPress', '', 'https://wordpress.org/', '', '2015-06-18 01:37:31', '2015-06-17 18:37:31', 'Hai, ini adalah sebuah komentar.\nUntuk menghapus komentar, cukup log masuk dan tampilkan komentar-komentar pada pos. Di situ ada opsi untuk menyunting atau menghapus mereka.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_links`
--

CREATE TABLE IF NOT EXISTS `mt_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_options`
--

CREATE TABLE IF NOT EXISTS `mt_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_options`
--

INSERT INTO `mt_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/maritime', 'yes'),
(2, 'home', 'http://localhost/maritime', 'yes'),
(3, 'blogname', 'Maritime Patrakom', 'yes'),
(4, 'blogdescription', 'Moto situs Anda bisa diletakkan di sini.', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hi@galihpratama.net', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:21:"jivochat/jivosite.php";i:1;s:39:"slate-admin-theme/slate-admin-theme.php";i:2;s:46:"woocommerce-bookings/woocommmerce-bookings.php";i:3;s:27:"woocommerce/woocommerce.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '7', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'maritime', 'yes'),
(42, 'stylesheet', 'maritime', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '0', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '31535', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '170', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '470', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '970', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '0', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '0', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:3:{i:2;a:3:{s:5:"title";s:17:"Maritime Patrakom";s:4:"text";s:351:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n<br><br>\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\n\r\n";s:6:"filter";b:0;}i:3;a:3:{s:5:"title";s:8:"Location";s:4:"text";s:256:"<strong>Jakarta</strong><br/>\r\nJl. Lorem Ipsum No. 19 Bandung<br/>\r\nKelurahan Antapani Kecamatan Dago<br/>\r\nIndonesia<br/>\r\n<br/>\r\n<strong>Bandung</strong><br/>\r\nJl. Dolor Sir Amet No. 19 Jakarta<br/>\r\nKelurahan Utan Kayu Kecamatan Matraman\r\nIndonesia<br/>";s:6:"filter";b:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:0:{}', 'yes'),
(82, 'uninstall_plugins', 'a:1:{s:44:"woocommerce-bookings/woocommerce-booking.php";s:31:"bkap_woocommerce_booking_delete";}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '31535', 'yes'),
(89, 'mt_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:133:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;s:15:"manage_bookings";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:3:{s:4:"read";b:1;s:10:"edit_posts";b:0;s:12:"delete_posts";b:0;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:111:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;s:15:"manage_bookings";b:1;}}}', 'yes'),
(90, 'WPLANG', '', 'yes'),
(91, 'widget_search', 'a:2:{i:3;a:1:{s:5:"title";s:6:"Search";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:19:"sidebar-widget-area";a:3:{i:0;s:8:"search-3";i:1;s:32:"woocommerce_top_rated_products-2";i:2;s:38:"woocommerce_recently_viewed_products-2";}s:18:"footer-widget-area";a:4:{i:0;s:6:"text-2";i:1;s:32:"woocommerce_product_categories-2";i:2;s:22:"woocommerce_products-2";i:3;s:6:"text-3";}s:13:"array_version";i:3;}', 'yes'),
(98, 'cron', 'a:8:{i:1435589848;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1435603052;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1435603150;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1435603316;a:2:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1435603353;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1435624320;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1435647600;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(109, '_transient_random_seed', 'a2e64e1cdb5ad1790a581a4af8f9d5be', 'yes'),
(113, 'can_compress_scripts', '1', 'yes'),
(132, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1434566350;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(133, 'current_theme', 'Maritime', 'yes'),
(134, 'theme_mods_maritime', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:18:"navbar-upper-right";i:6;s:17:"navbar-lower-left";i:7;s:18:"navbar-lower-right";i:8;}}', 'yes'),
(135, 'theme_switched', '', 'yes'),
(136, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"350";s:6:"height";s:3:"453";s:4:"crop";i:1;}', 'yes'),
(137, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"570";s:6:"height";s:3:"708";s:4:"crop";i:1;}', 'yes'),
(138, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"350";s:6:"height";s:3:"453";s:4:"crop";i:0;}', 'yes'),
(143, 'recently_activated', 'a:1:{s:44:"woocommerce-bookings/woocommerce-booking.php";i:1435583458;}', 'yes'),
(148, 'woocommerce_default_country', 'ID:JB', 'yes'),
(149, 'woocommerce_allowed_countries', 'all', 'yes'),
(150, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(151, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(152, 'woocommerce_demo_store', 'no', 'yes'),
(153, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes — no orders shall be fulfilled.', 'no'),
(154, 'woocommerce_api_enabled', 'yes', 'yes'),
(155, 'woocommerce_currency', 'IDR', 'yes'),
(156, 'woocommerce_currency_pos', 'left', 'yes'),
(157, 'woocommerce_price_thousand_sep', '.', 'yes'),
(158, 'woocommerce_price_decimal_sep', ',', 'yes'),
(159, 'woocommerce_price_num_decimals', '2', 'yes'),
(160, 'woocommerce_weight_unit', 'kg', 'yes'),
(161, 'woocommerce_dimension_unit', 'cm', 'yes'),
(162, 'woocommerce_enable_review_rating', 'yes', 'no'),
(163, 'woocommerce_review_rating_required', 'yes', 'no'),
(164, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(165, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(166, 'woocommerce_shop_page_id', '4', 'yes'),
(167, 'woocommerce_shop_page_display', '', 'yes'),
(168, 'woocommerce_category_archive_display', '', 'yes'),
(169, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(170, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(171, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(172, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(173, 'woocommerce_manage_stock', 'yes', 'yes'),
(174, 'woocommerce_hold_stock_minutes', '60', 'no'),
(175, 'woocommerce_notify_low_stock', 'yes', 'no'),
(176, 'woocommerce_notify_no_stock', 'yes', 'no'),
(177, 'woocommerce_stock_email_recipient', 'hi@galihpratama.net', 'no'),
(178, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(179, 'woocommerce_notify_no_stock_amount', '0', 'no'),
(180, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(181, 'woocommerce_stock_format', '', 'yes'),
(182, 'woocommerce_file_download_method', 'force', 'no'),
(183, 'woocommerce_downloads_require_login', 'no', 'no'),
(184, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(185, 'woocommerce_calc_taxes', 'no', 'yes'),
(186, 'woocommerce_prices_include_tax', 'no', 'yes'),
(187, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(188, 'woocommerce_shipping_tax_class', 'title', 'yes'),
(189, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(190, 'woocommerce_tax_classes', 'Reduced Rate\nZero Rate', 'yes'),
(191, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(192, 'woocommerce_tax_display_cart', 'excl', 'no'),
(193, 'woocommerce_price_display_suffix', '', 'yes'),
(194, 'woocommerce_tax_total_display', 'itemized', 'no'),
(195, 'woocommerce_enable_coupons', 'yes', 'no'),
(196, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(197, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(198, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(199, 'woocommerce_cart_page_id', '5', 'yes'),
(200, 'woocommerce_checkout_page_id', '6', 'yes'),
(201, 'woocommerce_terms_page_id', '', 'no'),
(202, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(203, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(204, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(205, 'woocommerce_calc_shipping', 'yes', 'yes'),
(206, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(207, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(208, 'woocommerce_shipping_method_format', '', 'no'),
(209, 'woocommerce_ship_to_destination', 'billing', 'no'),
(210, 'woocommerce_ship_to_countries', '', 'yes'),
(211, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(212, 'woocommerce_myaccount_page_id', '7', 'yes'),
(213, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(214, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(215, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(216, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(217, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(218, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(219, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(220, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(221, 'woocommerce_registration_generate_username', 'yes', 'no'),
(222, 'woocommerce_registration_generate_password', 'no', 'no'),
(223, 'woocommerce_email_from_name', 'Maritime', 'no'),
(224, 'woocommerce_email_from_address', 'hi@galihpratama.net', 'no'),
(225, 'woocommerce_email_header_image', '', 'no'),
(226, 'woocommerce_email_footer_text', 'Maritime - Powered by WooCommerce', 'no'),
(227, 'woocommerce_email_base_color', '#557da1', 'no'),
(228, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(229, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(230, 'woocommerce_email_text_color', '#505050', 'no'),
(232, 'woocommerce_db_version', '2.3.11', 'yes'),
(233, 'woocommerce_version', '2.3.11', 'yes'),
(234, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(239, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(240, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(242, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(243, '_transient_timeout_geoip_::1', '1435171334', 'no'),
(244, '_transient_geoip_::1', '', 'no'),
(245, '_transient_timeout_external_ip_address_::1', '1435171334', 'no'),
(246, '_transient_external_ip_address_::1', '36.72.32.234', 'no'),
(248, '_transient_timeout_geoip_36.72.32.234', '1435171335', 'no'),
(249, '_transient_geoip_36.72.32.234', 'ID', 'no'),
(256, 'woocommerce_allow_tracking', 'no', 'yes'),
(258, 'woocommerce_language_pack_version', 'a:2:{i:0;s:6:"2.3.11";i:1;s:5:"id_ID";}', 'yes'),
(262, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(263, '_wc_session_1', 'a:25:{s:4:"cart";s:618:"a:2:{s:32:"9bf31c7ff062936a96d3c8bd1f8f2ff3";a:9:{s:10:"product_id";i:15;s:12:"variation_id";s:0:"";s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:40000;s:8:"line_tax";i:0;s:13:"line_subtotal";i:40000;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}s:32:"d9d4f495e875a2e075a1a4a6e1b9770f";a:9:{s:10:"product_id";i:46;s:12:"variation_id";s:0:"";s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:20000;s:8:"line_tax";i:0;s:13:"line_subtotal";i:20000;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}";s:15:"applied_coupons";s:6:"a:0:{}";s:23:"coupon_discount_amounts";s:6:"a:0:{}";s:27:"coupon_discount_tax_amounts";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:19:"cart_contents_total";d:60000;s:20:"cart_contents_weight";i:0;s:19:"cart_contents_count";i:3;s:5:"total";d:60000;s:8:"subtotal";i:60000;s:15:"subtotal_ex_tax";i:60000;s:9:"tax_total";i:0;s:5:"taxes";s:6:"a:0:{}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";i:0;s:17:"discount_cart_tax";i:0;s:14:"shipping_total";i:0;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";s:10:"wc_notices";N;s:23:"chosen_shipping_methods";s:31:"a:1:{i:0;s:13:"free_shipping";}";s:22:"shipping_method_counts";s:14:"a:1:{i:0;i:1;}";s:21:"chosen_payment_method";s:4:"bacs";s:8:"customer";s:375:"a:14:{s:8:"postcode";s:0:"";s:4:"city";s:0:"";s:7:"address";s:0:"";s:9:"address_2";s:0:"";s:5:"state";s:0:"";s:7:"country";s:2:"ID";s:17:"shipping_postcode";s:0:"";s:13:"shipping_city";s:0:"";s:16:"shipping_address";s:0:"";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:0:"";s:16:"shipping_country";s:2:"ID";s:13:"is_vat_exempt";b:0;s:19:"calculated_shipping";b:0;}";}', 'no'),
(264, '_wc_session_expires_1', '1434739930', 'no'),
(265, '_transient_product_query-transient-version', '1435587764', 'yes'),
(268, 'category_children', 'a:0:{}', 'yes'),
(278, '_transient_product-transient-version', '1435587764', 'yes'),
(286, 'product_shipping_class_children', 'a:0:{}', 'yes'),
(307, '_transient_shipping-transient-version', '1434729045', 'yes'),
(308, '_transient_timeout_wc_ship_266547a0cadc33d172167747d0c585f5', '1434730877', 'no'),
(309, '_transient_wc_ship_266547a0cadc33d172167747d0c585f5', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:17:"Pengiriman Gratis";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(310, '_transient_timeout_wc_ship_7ba5184953028f993991204478d9c092', '1434730890', 'no'),
(311, '_transient_wc_ship_7ba5184953028f993991204478d9c092', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:17:"Pengiriman Gratis";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(321, 'woocommerce_permalinks', 'a:4:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:7:"/produk";}', 'yes'),
(337, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:58:"http://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:58:"http://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:10:"no_content";s:69:"http://downloads.wordpress.org/release/wordpress-4.2.2-no-content.zip";s:11:"new_bundled";s:70:"http://downloads.wordpress.org/release/wordpress-4.2.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1435582677;s:15:"version_checked";s:5:"4.2.2";s:12:"translations";a:0:{}}', 'yes'),
(339, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1435582696;s:7:"checked";a:4:{s:8:"maritime";s:5:"1.0.0";s:13:"twentyfifteen";s:3:"1.2";s:14:"twentyfourteen";s:3:"1.4";s:14:"twentythirteen";s:3:"1.5";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(346, '_site_transient_timeout_available_translations', '1434789227', 'yes'),
(347, '_site_transient_available_translations', 'a:56:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:37";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-01 14:30:22";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 06:36:25";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:22:"Продължение";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 18:55:51";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-24 05:23:15";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-30 08:59:10";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-03 00:26:43";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-19 15:30:54";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-19 21:59:02";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 13:39:01";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-04 14:48:26";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 17:53:27";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-04 19:47:01";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.0/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-16 10:01:41";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-15 10:49:37";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-12 09:59:32";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-05 17:37:43";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:59:"https://downloads.wordpress.org/translation/core/4.0/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:20:27";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.5/haz.zip";s:3:"iso";a:1:{i:2;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 19:32:58";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:12:"להמשיך";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 08:22:08";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:43:50";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:7:"Tovább";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:07:32";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 11:14:20";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 19:34:18";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-07 07:33:53";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:22";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ေဆာင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 10:29:43";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:59:29";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-08 07:10:14";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-10 17:07:58";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.2.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-09 10:15:05";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.1.5/ps.zip";s:3:"iso";a:1:{i:1;s:2:"ps";}s:7:"strings";a:1:{s:8:"continue";s:8:"دوام";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-27 09:25:14";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-12 01:38:15";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-18 17:18:37";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 11:58:44";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 09:29:23";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:25:46";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:10:"Nadaljujte";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 08:27:12";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-04 20:54:02";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:08:28";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 15:16:26";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:01:28";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-28 13:43:48";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-29 06:37:03";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'yes'),
(352, '_transient_timeout_wc_ship_a83d52e0301d24203e10550ab08b4f75', '1434782820', 'no'),
(353, '_transient_wc_ship_a83d52e0301d24203e10550ab08b4f75', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(360, '_transient_timeout_wc_ship_7a8dafdfadcac46fc60b0a2c69d69e2b', '1434786637', 'no'),
(361, '_transient_wc_ship_7a8dafdfadcac46fc60b0a2c69d69e2b', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no');
INSERT INTO `mt_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(364, 'rewrite_rules', 'a:213:{s:22:"^wc-api/v([1-2]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-2]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:7:"shop/?$";s:27:"index.php?post_type=product";s:37:"shop/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"shop/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"shop/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:12:"apf_posts/?$";s:29:"index.php?post_type=apf_posts";s:42:"apf_posts/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=apf_posts&feed=$matches[1]";s:37:"apf_posts/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=apf_posts&feed=$matches[1]";s:29:"apf_posts/page/([0-9]{1,})/?$";s:47:"index.php?post_type=apf_posts&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:55:"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:50:"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:43:"product-category/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:25:"product-category/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:52:"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:47:"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:40:"product-tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:22:"product-tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:34:"produk/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"produk/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"produk/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"produk/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"produk/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:27:"produk/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:47:"produk/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:42:"produk/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:35:"produk/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:42:"produk/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:32:"produk/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:38:"produk/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:49:"produk/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:27:"produk/([^/]+)(/[0-9]+)?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:23:"produk/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"produk/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"produk/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"produk/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"produk/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:38:"product_variation/([^/]+)(/[0-9]+)?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:45:"shop_order_refund/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"shop_order_refund/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"shop_order_refund/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"shop_order_refund/([^/]+)/trackback/?$";s:44:"index.php?shop_order_refund=$matches[1]&tb=1";s:46:"shop_order_refund/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&paged=$matches[2]";s:53:"shop_order_refund/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&cpage=$matches[2]";s:43:"shop_order_refund/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?shop_order_refund=$matches[1]&wc-api=$matches[3]";s:49:"shop_order_refund/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"shop_order_refund/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:38:"shop_order_refund/([^/]+)(/[0-9]+)?/?$";s:56:"index.php?shop_order_refund=$matches[1]&page=$matches[2]";s:34:"shop_order_refund/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"shop_order_refund/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"shop_order_refund/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:36:"resource/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"resource/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"resource/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"resource/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"resource/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"resource/([^/]+)/trackback/?$";s:35:"index.php?resource=$matches[1]&tb=1";s:37:"resource/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?resource=$matches[1]&paged=$matches[2]";s:44:"resource/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?resource=$matches[1]&cpage=$matches[2]";s:34:"resource/([^/]+)/wc-api(/(.*))?/?$";s:49:"index.php?resource=$matches[1]&wc-api=$matches[3]";s:40:"resource/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:51:"resource/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:29:"resource/([^/]+)(/[0-9]+)?/?$";s:47:"index.php?resource=$matches[1]&page=$matches[2]";s:25:"resource/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"resource/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"resource/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"resource/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"resource/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:60:"apf_sample_taxonomy/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:58:"index.php?apf_sample_taxonomy=$matches[1]&feed=$matches[2]";s:55:"apf_sample_taxonomy/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:58:"index.php?apf_sample_taxonomy=$matches[1]&feed=$matches[2]";s:48:"apf_sample_taxonomy/([^/]+)/page/?([0-9]{1,})/?$";s:59:"index.php?apf_sample_taxonomy=$matches[1]&paged=$matches[2]";s:30:"apf_sample_taxonomy/([^/]+)/?$";s:41:"index.php?apf_sample_taxonomy=$matches[1]";s:60:"apf_second_taxonomy/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:58:"index.php?apf_second_taxonomy=$matches[1]&feed=$matches[2]";s:55:"apf_second_taxonomy/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:58:"index.php?apf_second_taxonomy=$matches[1]&feed=$matches[2]";s:48:"apf_second_taxonomy/([^/]+)/page/?([0-9]{1,})/?$";s:59:"index.php?apf_second_taxonomy=$matches[1]&paged=$matches[2]";s:30:"apf_second_taxonomy/([^/]+)/?$";s:41:"index.php?apf_second_taxonomy=$matches[1]";s:37:"apf_posts/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"apf_posts/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"apf_posts/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"apf_posts/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"apf_posts/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"apf_posts/([^/]+)/trackback/?$";s:36:"index.php?apf_posts=$matches[1]&tb=1";s:50:"apf_posts/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?apf_posts=$matches[1]&feed=$matches[2]";s:45:"apf_posts/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?apf_posts=$matches[1]&feed=$matches[2]";s:38:"apf_posts/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?apf_posts=$matches[1]&paged=$matches[2]";s:45:"apf_posts/([^/]+)/comment-page-([0-9]{1,})/?$";s:49:"index.php?apf_posts=$matches[1]&cpage=$matches[2]";s:35:"apf_posts/([^/]+)/wc-api(/(.*))?/?$";s:50:"index.php?apf_posts=$matches[1]&wc-api=$matches[3]";s:41:"apf_posts/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:52:"apf_posts/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:30:"apf_posts/([^/]+)(/[0-9]+)?/?$";s:48:"index.php?apf_posts=$matches[1]&page=$matches[2]";s:26:"apf_posts/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"apf_posts/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"apf_posts/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"apf_posts/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"apf_posts/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:20:"order-pay(/(.*))?/?$";s:32:"index.php?&order-pay=$matches[2]";s:25:"order-received(/(.*))?/?$";s:37:"index.php?&order-received=$matches[2]";s:21:"view-order(/(.*))?/?$";s:33:"index.php?&view-order=$matches[2]";s:23:"edit-account(/(.*))?/?$";s:35:"index.php?&edit-account=$matches[2]";s:23:"edit-address(/(.*))?/?$";s:35:"index.php?&edit-address=$matches[2]";s:24:"lost-password(/(.*))?/?$";s:36:"index.php?&lost-password=$matches[2]";s:26:"customer-logout(/(.*))?/?$";s:38:"index.php?&customer-logout=$matches[2]";s:29:"add-payment-method(/(.*))?/?$";s:41:"index.php?&add-payment-method=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(365, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:3:"p=5";i:1;s:5:"/cart";i:2;s:3:"p=6";i:3;s:9:"/checkout";i:4;s:3:"p=7";i:5;s:11:"/my-account";}', 'yes'),
(393, '_site_transient_timeout_browser_182ac12c95cf1d0594d9db44179afdd5', '1436157311', 'yes'),
(394, '_site_transient_browser_182ac12c95cf1d0594d9db44179afdd5', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"43.0.2357.124";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(399, '_transient_timeout_wc_admin_report', '1435638911', 'no'),
(400, '_transient_wc_admin_report', 'a:1:{s:32:"ff8014ae713f1c542b2d4201d78970df";a:0:{}}', 'no'),
(416, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(417, 'widget_woocommerce_top_rated_products', 'a:2:{i:2;a:2:{s:5:"title";s:18:"Top Rated Products";s:6:"number";s:1:"5";}s:12:"_multiwidget";i:1;}', 'yes'),
(418, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:6:{s:5:"title";s:10:"Categories";s:7:"orderby";s:4:"name";s:8:"dropdown";i:0;s:5:"count";i:0;s:12:"hierarchical";s:1:"1";s:18:"show_children_only";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(419, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(424, 'widget_woocommerce_recently_viewed_products', 'a:2:{i:2;a:2:{s:5:"title";s:24:"Recently Viewed Products";s:6:"number";s:2:"10";}s:12:"_multiwidget";i:1;}', 'yes'),
(425, 'product_cat_children', 'a:2:{i:10;a:6:{i:0;i:9;i:1;i:11;i:2;i:12;i:3;i:13;i:4;i:14;i:5;i:15;}i:16;a:2:{i:0;i:17;i:1;i:18;}}', 'yes'),
(426, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1435564484', 'yes'),
(427, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5223";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3269";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3204";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"2734";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2503";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2001";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1906";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1836";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1787";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1769";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1738";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1728";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1621";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1419";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1357";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1299";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1207";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1165";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1150";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1021";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"975";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:3:"942";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"932";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"896";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"865";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"853";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"806";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"791";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"767";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"743";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"738";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"736";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"695";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"687";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"682";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"669";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"649";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"645";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"640";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"639";}}', 'yes'),
(429, 'jivosite_widget_id', 'c5mB6lYzew', 'yes'),
(430, '_transient_timeout_plugin_slugs', '1435670449', 'no'),
(431, '_transient_plugin_slugs', 'a:5:{i:0;s:19:"akismet/akismet.php";i:1;s:21:"jivochat/jivosite.php";i:2;s:39:"slate-admin-theme/slate-admin-theme.php";i:3;s:27:"woocommerce/woocommerce.php";i:4;s:46:"woocommerce-bookings/woocommmerce-bookings.php";}', 'no'),
(436, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1435584038;s:7:"checked";a:5:{s:19:"akismet/akismet.php";s:5:"3.1.2";s:21:"jivochat/jivosite.php";s:3:"1.1";s:39:"slate-admin-theme/slate-admin-theme.php";s:5:"1.1.4";s:27:"woocommerce/woocommerce.php";s:6:"2.3.11";s:46:"woocommerce-bookings/woocommmerce-bookings.php";s:5:"1.4.9";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:4:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.1.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.1.2.zip";}s:21:"jivochat/jivosite.php";O:8:"stdClass":6:{s:2:"id";s:5:"53666";s:4:"slug";s:8:"jivochat";s:6:"plugin";s:21:"jivochat/jivosite.php";s:11:"new_version";s:3:"1.1";s:3:"url";s:39:"https://wordpress.org/plugins/jivochat/";s:7:"package";s:55:"https://downloads.wordpress.org/plugin/jivochat.1.1.zip";}s:39:"slate-admin-theme/slate-admin-theme.php";O:8:"stdClass":7:{s:2:"id";s:5:"53593";s:4:"slug";s:17:"slate-admin-theme";s:6:"plugin";s:39:"slate-admin-theme/slate-admin-theme.php";s:11:"new_version";s:5:"1.1.4";s:3:"url";s:48:"https://wordpress.org/plugins/slate-admin-theme/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/slate-admin-theme.1.1.4.zip";s:14:"upgrade_notice";s:62:"Fixed a bug regarding hidden post titles for Subscriber roles.";}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":6:{s:2:"id";s:5:"25331";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:6:"2.3.11";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/woocommerce.2.3.11.zip";}}}', 'yes'),
(437, '_site_transient_timeout_theme_roots', '1435584481', 'yes'),
(438, '_site_transient_theme_roots', 'a:4:{s:8:"maritime";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";}', 'yes'),
(440, 'woocommerce_booking_db_version', '2.2', 'yes'),
(441, 'woocommerce_booking_abp_hrs', 'HOURS', 'yes'),
(442, 'book.date-label', 'Start Date', 'yes'),
(443, 'checkout.date-label', '<br>End Date', 'yes'),
(444, 'book.time-label', 'Booking Time', 'yes'),
(445, 'book.time-select-option', 'Choose a Time', 'yes'),
(446, 'book.item-meta-date', 'Start Date', 'yes'),
(447, 'checkout.item-meta-date', 'End Date', 'yes'),
(448, 'book.item-meta-time', 'Booking Time', 'yes'),
(449, 'book.ics-file-name', 'Mycal', 'yes'),
(450, 'book.item-cart-date', 'Start Date', 'yes'),
(451, 'checkout.item-cart-date', 'End Date', 'yes'),
(452, 'book.item-cart-time', 'Booking Time', 'yes'),
(453, 'widget_woocommerce_products', 'a:2:{i:2;a:7:{s:5:"title";s:14:"Newest Product";s:6:"number";s:1:"5";s:4:"show";s:0:"";s:7:"orderby";s:4:"date";s:5:"order";s:4:"desc";s:9:"hide_free";i:0;s:11:"show_hidden";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(454, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(455, 'wc_bookings_version', '1.4.9', 'yes'),
(456, '_transient_timeout_wc_ship_d53c009e8d6d999348f7a2f713628a58', '1435588301', 'no'),
(457, '_transient_wc_ship_d53c009e8d6d999348f7a2f713628a58', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(458, '_transient_timeout_wc_ship_f77b8efccf3830575d952d09cf058d36', '1435588321', 'no'),
(459, '_transient_wc_ship_f77b8efccf3830575d952d09cf058d36', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(504, '_transient_timeout_wc_ship_35cf09dfef282e2f5b2e122bfcda42a3', '1435589064', 'no'),
(505, '_transient_wc_ship_35cf09dfef282e2f5b2e122bfcda42a3', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(565, '_transient_timeout_wc_term_counts', '1438179767', 'no'),
(566, '_transient_wc_term_counts', 'a:11:{i:19;s:1:"3";i:9;s:1:"2";i:12;s:1:"3";i:10;s:1:"6";i:11;s:1:"1";i:17;s:0:"";i:13;s:0:"";i:15;s:0:"";i:16;s:1:"0";i:18;s:0:"";i:14;s:0:"";}', 'no'),
(567, '_transient_timeout_wc_ship_1e36189ec5c450a01d55879b22699a73', '1435591923', 'no'),
(568, '_transient_wc_ship_1e36189ec5c450a01d55879b22699a73', 'a:1:{s:13:"free_shipping";O:16:"WC_Shipping_Rate":5:{s:2:"id";s:13:"free_shipping";s:5:"label";s:13:"Free Shipping";s:4:"cost";i:0;s:5:"taxes";a:0:{}s:9:"method_id";s:13:"free_shipping";}}', 'no'),
(569, '_transient_timeout_wc_rating_count_501435587764', '1438180323', 'no'),
(570, '_transient_wc_rating_count_501435587764', 'a:0:{}', 'no'),
(571, '_transient_timeout_wc_average_rating_501435587764', '1438180323', 'no'),
(572, '_transient_wc_average_rating_501435587764', '', 'no'),
(573, '_transient_timeout_wc_rating_count_491435587764', '1438180323', 'no'),
(574, '_transient_wc_rating_count_491435587764', 'a:0:{}', 'no'),
(575, '_transient_timeout_wc_average_rating_491435587764', '1438180323', 'no'),
(576, '_transient_wc_average_rating_491435587764', '', 'no'),
(577, '_transient_timeout_wc_rating_count_481435587764', '1438180323', 'no'),
(578, '_transient_wc_rating_count_481435587764', 'a:0:{}', 'no'),
(579, '_transient_timeout_wc_average_rating_481435587764', '1438180323', 'no'),
(580, '_transient_wc_average_rating_481435587764', '', 'no'),
(581, '_transient_timeout_wc_rating_count_471435587764', '1438180323', 'no'),
(582, '_transient_wc_rating_count_471435587764', 'a:0:{}', 'no'),
(583, '_transient_timeout_wc_average_rating_471435587764', '1438180323', 'no'),
(584, '_transient_wc_average_rating_471435587764', '', 'no'),
(585, '_transient_timeout_wc_rating_count_461435587764', '1438180323', 'no'),
(586, '_transient_wc_rating_count_461435587764', 'a:0:{}', 'no'),
(587, '_transient_timeout_wc_average_rating_461435587764', '1438180323', 'no'),
(588, '_transient_wc_average_rating_461435587764', '', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `mt_postmeta`
--

CREATE TABLE IF NOT EXISTS `mt_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_postmeta`
--

INSERT INTO `mt_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 10, '_menu_item_type', 'post_type'),
(3, 10, '_menu_item_menu_item_parent', '0'),
(4, 10, '_menu_item_object_id', '7'),
(5, 10, '_menu_item_object', 'page'),
(6, 10, '_menu_item_target', ''),
(7, 10, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(8, 10, '_menu_item_xfn', ''),
(9, 10, '_menu_item_url', ''),
(11, 11, '_menu_item_type', 'post_type'),
(12, 11, '_menu_item_menu_item_parent', '0'),
(13, 11, '_menu_item_object_id', '6'),
(14, 11, '_menu_item_object', 'page'),
(15, 11, '_menu_item_target', ''),
(16, 11, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(17, 11, '_menu_item_xfn', ''),
(18, 11, '_menu_item_url', ''),
(20, 12, '_menu_item_type', 'post_type'),
(21, 12, '_menu_item_menu_item_parent', '0'),
(22, 12, '_menu_item_object_id', '5'),
(23, 12, '_menu_item_object', 'page'),
(24, 12, '_menu_item_target', ''),
(25, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(26, 12, '_menu_item_xfn', ''),
(27, 12, '_menu_item_url', ''),
(46, 15, '_edit_last', '1'),
(47, 15, '_edit_lock', '1435587436:1'),
(48, 16, '_wp_attached_file', '2015/06/Screen-Shot-2015-06-19-at-22.17.30.png'),
(49, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:517;s:6:"height";i:402;s:4:"file";s:46:"2015/06/Screen-Shot-2015-06-19-at-22.17.30.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.17.30-170x150.png";s:5:"width";i:170;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.17.30-386x300.png";s:5:"width";i:386;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.17.30-350x272.png";s:5:"width";i:350;s:6:"height";i:272;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.17.30-350x402.png";s:5:"width";i:350;s:6:"height";i:402;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(50, 15, '_thumbnail_id', '16'),
(51, 15, '_visibility', 'visible'),
(52, 15, '_stock_status', 'instock'),
(53, 15, 'total_sales', '0'),
(54, 15, '_downloadable', 'no'),
(55, 15, '_virtual', 'no'),
(56, 15, '_regular_price', '20000'),
(57, 15, '_sale_price', ''),
(58, 15, '_purchase_note', ''),
(59, 15, '_featured', 'no'),
(60, 15, '_weight', ''),
(61, 15, '_length', ''),
(62, 15, '_width', ''),
(63, 15, '_height', ''),
(64, 15, '_sku', 'IPSUM'),
(65, 15, '_product_attributes', 'a:0:{}'),
(66, 15, '_sale_price_dates_from', ''),
(67, 15, '_sale_price_dates_to', ''),
(68, 15, '_price', '20000'),
(69, 15, '_sold_individually', ''),
(70, 15, '_manage_stock', 'no'),
(71, 15, '_backorders', 'no'),
(72, 15, '_stock', ''),
(73, 15, '_upsell_ids', 'a:0:{}'),
(74, 15, '_crosssell_ids', 'a:0:{}'),
(75, 15, '_product_image_gallery', '17,16,18'),
(76, 17, '_wp_attached_file', '2015/06/iridium-go-003.jpg'),
(77, 17, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:472;s:6:"height";i:472;s:4:"file";s:26:"2015/06/iridium-go-003.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"iridium-go-003-170x150.jpg";s:5:"width";i:170;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"iridium-go-003-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"iridium-go-003-350x350.jpg";s:5:"width";i:350;s:6:"height";i:350;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"iridium-go-003-350x453.jpg";s:5:"width";i:350;s:6:"height";i:453;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(78, 18, '_wp_attached_file', '2015/06/Screen-Shot-2015-06-19-at-22.20.16.png'),
(79, 18, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:636;s:6:"height";i:499;s:4:"file";s:46:"2015/06/Screen-Shot-2015-06-19-at-22.20.16.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.20.16-170x150.png";s:5:"width";i:170;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.20.16-382x300.png";s:5:"width";i:382;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.20.16-350x275.png";s:5:"width";i:350;s:6:"height";i:275;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.20.16-350x453.png";s:5:"width";i:350;s:6:"height";i:453;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:46:"Screen-Shot-2015-06-19-at-22.20.16-570x499.png";s:5:"width";i:570;s:6:"height";i:499;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(116, 23, '_menu_item_type', 'post_type'),
(117, 23, '_menu_item_menu_item_parent', '0'),
(118, 23, '_menu_item_object_id', '5'),
(119, 23, '_menu_item_object', 'page'),
(120, 23, '_menu_item_target', ''),
(121, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(122, 23, '_menu_item_xfn', ''),
(123, 23, '_menu_item_url', ''),
(125, 24, '_menu_item_type', 'custom'),
(126, 24, '_menu_item_menu_item_parent', '0'),
(127, 24, '_menu_item_object_id', '24'),
(128, 24, '_menu_item_object', 'custom'),
(129, 24, '_menu_item_target', ''),
(130, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(131, 24, '_menu_item_xfn', ''),
(132, 24, '_menu_item_url', 'http://localhost/maritime/'),
(134, 26, '_menu_item_type', 'taxonomy'),
(135, 26, '_menu_item_menu_item_parent', '0'),
(136, 26, '_menu_item_object_id', '16'),
(137, 26, '_menu_item_object', 'product_cat'),
(138, 26, '_menu_item_target', ''),
(139, 26, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(140, 26, '_menu_item_xfn', ''),
(141, 26, '_menu_item_url', ''),
(143, 27, '_menu_item_type', 'taxonomy'),
(144, 27, '_menu_item_menu_item_parent', '26'),
(145, 27, '_menu_item_object_id', '17'),
(146, 27, '_menu_item_object', 'product_cat'),
(147, 27, '_menu_item_target', ''),
(148, 27, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(149, 27, '_menu_item_xfn', ''),
(150, 27, '_menu_item_url', ''),
(152, 28, '_menu_item_type', 'taxonomy'),
(153, 28, '_menu_item_menu_item_parent', '26'),
(154, 28, '_menu_item_object_id', '18'),
(155, 28, '_menu_item_object', 'product_cat'),
(156, 28, '_menu_item_target', ''),
(157, 28, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(158, 28, '_menu_item_xfn', ''),
(159, 28, '_menu_item_url', ''),
(161, 29, '_menu_item_type', 'taxonomy'),
(162, 29, '_menu_item_menu_item_parent', '0'),
(163, 29, '_menu_item_object_id', '10'),
(164, 29, '_menu_item_object', 'product_cat'),
(165, 29, '_menu_item_target', ''),
(166, 29, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(167, 29, '_menu_item_xfn', ''),
(168, 29, '_menu_item_url', ''),
(170, 30, '_menu_item_type', 'taxonomy'),
(171, 30, '_menu_item_menu_item_parent', '29'),
(172, 30, '_menu_item_object_id', '13'),
(173, 30, '_menu_item_object', 'product_cat'),
(174, 30, '_menu_item_target', ''),
(175, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(176, 30, '_menu_item_xfn', ''),
(177, 30, '_menu_item_url', ''),
(179, 31, '_menu_item_type', 'taxonomy'),
(180, 31, '_menu_item_menu_item_parent', '29'),
(181, 31, '_menu_item_object_id', '9'),
(182, 31, '_menu_item_object', 'product_cat'),
(183, 31, '_menu_item_target', ''),
(184, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(185, 31, '_menu_item_xfn', ''),
(186, 31, '_menu_item_url', ''),
(188, 32, '_menu_item_type', 'taxonomy'),
(189, 32, '_menu_item_menu_item_parent', '29'),
(190, 32, '_menu_item_object_id', '15'),
(191, 32, '_menu_item_object', 'product_cat'),
(192, 32, '_menu_item_target', ''),
(193, 32, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(194, 32, '_menu_item_xfn', ''),
(195, 32, '_menu_item_url', ''),
(197, 33, '_menu_item_type', 'taxonomy'),
(198, 33, '_menu_item_menu_item_parent', '29'),
(199, 33, '_menu_item_object_id', '12'),
(200, 33, '_menu_item_object', 'product_cat'),
(201, 33, '_menu_item_target', ''),
(202, 33, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(203, 33, '_menu_item_xfn', ''),
(204, 33, '_menu_item_url', ''),
(206, 34, '_menu_item_type', 'taxonomy'),
(207, 34, '_menu_item_menu_item_parent', '29'),
(208, 34, '_menu_item_object_id', '14'),
(209, 34, '_menu_item_object', 'product_cat'),
(210, 34, '_menu_item_target', ''),
(211, 34, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(212, 34, '_menu_item_xfn', ''),
(213, 34, '_menu_item_url', ''),
(215, 35, '_menu_item_type', 'taxonomy'),
(216, 35, '_menu_item_menu_item_parent', '29'),
(217, 35, '_menu_item_object_id', '11'),
(218, 35, '_menu_item_object', 'product_cat'),
(219, 35, '_menu_item_target', ''),
(220, 35, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(221, 35, '_menu_item_xfn', ''),
(222, 35, '_menu_item_url', ''),
(224, 36, '_menu_item_type', 'taxonomy'),
(225, 36, '_menu_item_menu_item_parent', '0'),
(226, 36, '_menu_item_object_id', '19'),
(227, 36, '_menu_item_object', 'product_cat'),
(228, 36, '_menu_item_target', ''),
(229, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(230, 36, '_menu_item_xfn', ''),
(231, 36, '_menu_item_url', ''),
(235, 38, '_edit_last', '1'),
(236, 38, '_edit_lock', '1435585538:1'),
(237, 38, '_wp_page_template', 'page-full-width.php'),
(238, 41, '_menu_item_type', 'post_type'),
(239, 41, '_menu_item_menu_item_parent', '0'),
(240, 41, '_menu_item_object_id', '38'),
(241, 41, '_menu_item_object', 'page'),
(242, 41, '_menu_item_target', ''),
(243, 41, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(244, 41, '_menu_item_xfn', ''),
(245, 41, '_menu_item_url', ''),
(247, 42, '_edit_last', '1'),
(248, 42, '_edit_lock', '1435587430:1'),
(249, 42, '_thumbnail_id', '16'),
(250, 42, '_visibility', 'visible'),
(251, 42, '_stock_status', 'instock'),
(252, 42, '_downloadable', 'no'),
(253, 42, '_virtual', 'no'),
(254, 42, '_regular_price', '20000'),
(255, 42, '_sale_price', ''),
(256, 42, '_purchase_note', ''),
(257, 42, '_featured', 'no'),
(258, 42, '_weight', ''),
(259, 42, '_length', ''),
(260, 42, '_width', ''),
(261, 42, '_height', ''),
(262, 42, '_sku', 'IPSUM'),
(263, 42, '_product_attributes', 'a:0:{}'),
(264, 42, '_sale_price_dates_from', ''),
(265, 42, '_sale_price_dates_to', ''),
(266, 42, '_price', '20000'),
(267, 42, '_sold_individually', ''),
(268, 42, '_manage_stock', 'no'),
(269, 42, '_backorders', 'no'),
(270, 42, '_stock', ''),
(271, 42, '_upsell_ids', 'a:0:{}'),
(272, 42, '_crosssell_ids', 'a:0:{}'),
(273, 42, '_product_image_gallery', '17,16,18'),
(278, 42, 'total_sales', '0'),
(279, 43, '_edit_last', '1'),
(280, 43, '_edit_lock', '1435587423:1'),
(281, 43, '_thumbnail_id', '16'),
(282, 43, '_visibility', 'visible'),
(283, 43, '_stock_status', 'instock'),
(284, 43, '_downloadable', 'no'),
(285, 43, '_virtual', 'no'),
(286, 43, '_regular_price', '20000'),
(287, 43, '_sale_price', ''),
(288, 43, '_purchase_note', ''),
(289, 43, '_featured', 'no'),
(290, 43, '_weight', ''),
(291, 43, '_length', ''),
(292, 43, '_width', ''),
(293, 43, '_height', ''),
(294, 43, '_sku', 'IPSUM'),
(295, 43, '_product_attributes', 'a:0:{}'),
(296, 43, '_sale_price_dates_from', ''),
(297, 43, '_sale_price_dates_to', ''),
(298, 43, '_price', '20000'),
(299, 43, '_sold_individually', ''),
(300, 43, '_manage_stock', 'no'),
(301, 43, '_backorders', 'no'),
(302, 43, '_stock', ''),
(303, 43, '_upsell_ids', 'a:0:{}'),
(304, 43, '_crosssell_ids', 'a:0:{}'),
(305, 43, '_product_image_gallery', '17,16,18'),
(310, 43, 'total_sales', '0'),
(311, 45, '_edit_last', '1'),
(312, 45, '_edit_lock', '1435585276:1'),
(313, 45, '_thumbnail_id', '16'),
(314, 45, '_visibility', 'visible'),
(315, 45, '_stock_status', 'instock'),
(316, 45, '_downloadable', 'no'),
(317, 45, '_virtual', 'no'),
(318, 45, '_regular_price', '20000'),
(319, 45, '_sale_price', ''),
(320, 45, '_purchase_note', ''),
(321, 45, '_featured', 'no'),
(322, 45, '_weight', ''),
(323, 45, '_length', ''),
(324, 45, '_width', ''),
(325, 45, '_height', ''),
(326, 45, '_sku', 'IPSUM'),
(327, 45, '_product_attributes', 'a:0:{}'),
(328, 45, '_sale_price_dates_from', ''),
(329, 45, '_sale_price_dates_to', ''),
(330, 45, '_price', '20000'),
(331, 45, '_sold_individually', ''),
(332, 45, '_manage_stock', 'no'),
(333, 45, '_backorders', 'no'),
(334, 45, '_stock', ''),
(335, 45, '_upsell_ids', 'a:0:{}'),
(336, 45, '_crosssell_ids', 'a:0:{}'),
(337, 45, '_product_image_gallery', '17,16,18'),
(342, 45, 'total_sales', '0'),
(343, 46, '_edit_last', '1'),
(344, 46, '_edit_lock', '1435585255:1'),
(345, 46, '_thumbnail_id', '16'),
(346, 46, '_visibility', 'visible'),
(347, 46, '_stock_status', 'instock'),
(348, 46, '_downloadable', 'no'),
(349, 46, '_virtual', 'no'),
(350, 46, '_regular_price', '20000'),
(351, 46, '_sale_price', ''),
(352, 46, '_purchase_note', ''),
(353, 46, '_featured', 'no'),
(354, 46, '_weight', ''),
(355, 46, '_length', ''),
(356, 46, '_width', ''),
(357, 46, '_height', ''),
(358, 46, '_sku', 'IPSUM'),
(359, 46, '_product_attributes', 'a:0:{}'),
(360, 46, '_sale_price_dates_from', ''),
(361, 46, '_sale_price_dates_to', ''),
(362, 46, '_price', '20000'),
(363, 46, '_sold_individually', ''),
(364, 46, '_manage_stock', 'no'),
(365, 46, '_backorders', 'no'),
(366, 46, '_stock', ''),
(367, 46, '_upsell_ids', 'a:0:{}'),
(368, 46, '_crosssell_ids', 'a:0:{}'),
(369, 46, '_product_image_gallery', '17,16,18'),
(374, 46, 'total_sales', '0'),
(375, 47, '_edit_last', '1'),
(376, 47, '_edit_lock', '1435585240:1'),
(377, 47, '_thumbnail_id', '16'),
(378, 47, '_visibility', 'visible'),
(379, 47, '_stock_status', 'instock'),
(380, 47, '_downloadable', 'no'),
(381, 47, '_virtual', 'no'),
(382, 47, '_regular_price', '20000'),
(383, 47, '_sale_price', ''),
(384, 47, '_purchase_note', ''),
(385, 47, '_featured', 'no'),
(386, 47, '_weight', ''),
(387, 47, '_length', ''),
(388, 47, '_width', ''),
(389, 47, '_height', ''),
(390, 47, '_sku', 'IPSUM'),
(391, 47, '_product_attributes', 'a:0:{}'),
(392, 47, '_sale_price_dates_from', ''),
(393, 47, '_sale_price_dates_to', ''),
(394, 47, '_price', '20000'),
(395, 47, '_sold_individually', ''),
(396, 47, '_manage_stock', 'no'),
(397, 47, '_backorders', 'no'),
(398, 47, '_stock', ''),
(399, 47, '_upsell_ids', 'a:0:{}'),
(400, 47, '_crosssell_ids', 'a:0:{}'),
(401, 47, '_product_image_gallery', '17,16,18'),
(406, 47, 'total_sales', '0'),
(407, 47, '_tax_status', 'taxable'),
(408, 47, '_tax_class', ''),
(409, 46, '_tax_status', 'taxable'),
(410, 46, '_tax_class', ''),
(411, 45, '_tax_status', 'taxable'),
(412, 45, '_tax_class', ''),
(413, 43, '_tax_status', 'taxable'),
(414, 43, '_tax_class', ''),
(415, 42, '_tax_status', 'taxable'),
(416, 42, '_tax_class', ''),
(417, 15, '_tax_status', 'taxable'),
(418, 15, '_tax_class', ''),
(419, 48, '_edit_last', '1'),
(420, 48, '_edit_lock', '1435587580:1'),
(421, 48, '_thumbnail_id', '16'),
(422, 48, '_visibility', 'visible'),
(423, 48, '_stock_status', 'instock'),
(424, 48, '_downloadable', 'no'),
(425, 48, '_virtual', 'no'),
(426, 48, '_regular_price', '20000'),
(427, 48, '_sale_price', ''),
(428, 48, '_purchase_note', ''),
(429, 48, '_featured', 'no'),
(430, 48, '_weight', ''),
(431, 48, '_length', ''),
(432, 48, '_width', ''),
(433, 48, '_height', ''),
(434, 48, '_sku', 'IPSUM'),
(435, 48, '_product_attributes', 'a:0:{}'),
(436, 48, '_sale_price_dates_from', ''),
(437, 48, '_sale_price_dates_to', ''),
(438, 48, '_price', '20000'),
(439, 48, '_sold_individually', ''),
(440, 48, '_manage_stock', 'no'),
(441, 48, '_backorders', 'no'),
(442, 48, '_stock', ''),
(443, 48, '_upsell_ids', 'a:0:{}'),
(444, 48, '_crosssell_ids', 'a:0:{}'),
(445, 48, '_product_image_gallery', '17,16,18'),
(446, 48, '_tax_status', 'taxable'),
(447, 48, '_tax_class', ''),
(450, 48, 'total_sales', '0'),
(451, 49, '_edit_last', '1'),
(452, 49, '_edit_lock', '1435587597:1'),
(453, 49, '_thumbnail_id', '16'),
(454, 49, '_visibility', 'visible'),
(455, 49, '_stock_status', 'instock'),
(456, 49, '_downloadable', 'no'),
(457, 49, '_virtual', 'no'),
(458, 49, '_regular_price', '20000'),
(459, 49, '_sale_price', ''),
(460, 49, '_purchase_note', ''),
(461, 49, '_featured', 'no'),
(462, 49, '_weight', ''),
(463, 49, '_length', ''),
(464, 49, '_width', ''),
(465, 49, '_height', ''),
(466, 49, '_sku', 'IPSUM'),
(467, 49, '_product_attributes', 'a:0:{}'),
(468, 49, '_sale_price_dates_from', ''),
(469, 49, '_sale_price_dates_to', ''),
(470, 49, '_price', '20000'),
(471, 49, '_sold_individually', ''),
(472, 49, '_manage_stock', 'no'),
(473, 49, '_backorders', 'no'),
(474, 49, '_stock', ''),
(475, 49, '_upsell_ids', 'a:0:{}'),
(476, 49, '_crosssell_ids', 'a:0:{}'),
(477, 49, '_product_image_gallery', '17,16,18'),
(478, 49, '_tax_status', 'taxable'),
(479, 49, '_tax_class', ''),
(482, 49, 'total_sales', '0'),
(483, 50, '_edit_last', '1'),
(484, 50, '_edit_lock', '1435587681:1'),
(485, 50, '_thumbnail_id', '16'),
(486, 50, '_visibility', 'visible'),
(487, 50, '_stock_status', 'instock'),
(488, 50, '_downloadable', 'no'),
(489, 50, '_virtual', 'no'),
(490, 50, '_regular_price', '20000'),
(491, 50, '_sale_price', ''),
(492, 50, '_purchase_note', ''),
(493, 50, '_featured', 'no'),
(494, 50, '_weight', ''),
(495, 50, '_length', ''),
(496, 50, '_width', ''),
(497, 50, '_height', ''),
(498, 50, '_sku', 'IPSUM'),
(499, 50, '_product_attributes', 'a:0:{}'),
(500, 50, '_sale_price_dates_from', ''),
(501, 50, '_sale_price_dates_to', ''),
(502, 50, '_price', '20000'),
(503, 50, '_sold_individually', ''),
(504, 50, '_manage_stock', 'no'),
(505, 50, '_backorders', 'no'),
(506, 50, '_stock', ''),
(507, 50, '_upsell_ids', 'a:0:{}'),
(508, 50, '_crosssell_ids', 'a:0:{}'),
(509, 50, '_product_image_gallery', '17,16,18'),
(510, 50, '_tax_status', 'taxable'),
(511, 50, '_tax_class', ''),
(514, 50, 'total_sales', '0');

-- --------------------------------------------------------

--
-- Table structure for table `mt_posts`
--

CREATE TABLE IF NOT EXISTS `mt_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_posts`
--

INSERT INTO `mt_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2015-06-18 01:37:31', '2015-06-17 18:37:31', 'Selamat datang di WordPress. Ini adalah tulisan pertama Anda. Sunting atau hapus, kemudian mulai blogging!', 'Halo dunia!', '', 'publish', 'open', 'open', '', 'halo-dunia', '', '', '2015-06-18 01:37:31', '2015-06-17 18:37:31', '', 0, 'http://localhost/maritime/?p=1', 0, 'post', '', 1),
(2, 1, '2015-06-18 01:37:31', '2015-06-17 18:37:31', 'Ini adalah sebuah contoh Laman. Berbeda dari sebuah Pos blog sebab laman semacam ini memiliki lokasi tetap, dan memiliki sebuah tautan pada menu navigasi situs Anda (pada sebagian tema). Biasanya, penulis memulai dengan menulis laman Tentang untuk memperkenalkan dirinya pada para calon pengunjung situs. Isinya dapat berupa:\n\n<blockquote>Hai! Saya adalah seorang kurir jasa paket yang hobi nge-blog. Lahir dan besar di Indonesia, sejak SMA sudah jatuh cinta pada jazz (musiknya orang WordPress)</blockquote>\n\n...atau seperti ini:\n\n<blockquote>Automattic Inc. adalah perusahaan perangkat lunak yang didirikan pada bulan Agustus 2005 oleh Matt Mullenwegg (pelopor WordPress). Berkantor pusat di Amerika Serikat, perusahaan kami mempekerjakan ratusan programmer yang tersebar di 5 benua.</blockquote>\n\nSebagai pengguna baru di WordPress, Anda sebaiknya segera membuka <a href="http://localhost/maritime/wp-admin/">Dasbor Anda</a> untuk menghapus laman ini dan membuat laman baru sesuai keinginan Anda. Have fun!', 'Laman Contoh', '', 'publish', 'open', 'open', '', 'laman-contoh', '', '', '2015-06-18 01:37:31', '2015-06-17 18:37:31', '', 0, 'http://localhost/maritime/?page_id=2', 0, 'page', '', 0),
(4, 1, '2015-06-18 01:42:14', '2015-06-17 18:42:14', '', 'Shop', '', 'publish', 'closed', 'open', '', 'shop', '', '', '2015-06-18 01:42:14', '2015-06-17 18:42:14', '', 0, 'http://localhost/maritime/shop/', 0, 'page', '', 0),
(5, 1, '2015-06-18 01:42:14', '2015-06-17 18:42:14', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'open', '', 'cart', '', '', '2015-06-18 01:42:14', '2015-06-17 18:42:14', '', 0, 'http://localhost/maritime/cart/', 0, 'page', '', 0),
(6, 1, '2015-06-18 01:42:14', '2015-06-17 18:42:14', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'open', '', 'checkout', '', '', '2015-06-18 01:42:14', '2015-06-17 18:42:14', '', 0, 'http://localhost/maritime/checkout/', 0, 'page', '', 0),
(7, 1, '2015-06-18 01:42:14', '2015-06-17 18:42:14', '[woocommerce_my_account]', 'My Account', '', 'publish', 'closed', 'open', '', 'my-account', '', '', '2015-06-18 01:42:14', '2015-06-17 18:42:14', '', 0, 'http://localhost/maritime/my-account/', 0, 'page', '', 0),
(10, 1, '2015-06-18 01:51:59', '2015-06-17 18:51:59', ' ', '', '', 'publish', 'open', 'open', '', '10', '', '', '2015-06-20 12:06:47', '2015-06-20 05:06:47', '', 0, 'http://localhost/maritime/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2015-06-18 01:51:59', '2015-06-17 18:51:59', ' ', '', '', 'publish', 'open', 'open', '', '11', '', '', '2015-06-20 12:06:47', '2015-06-20 05:06:47', '', 0, 'http://localhost/maritime/?p=11', 3, 'nav_menu_item', '', 0),
(12, 1, '2015-06-18 01:51:59', '2015-06-17 18:51:59', '', 'Shopping Cart', '', 'publish', 'open', 'open', '', '12', '', '', '2015-06-20 12:06:47', '2015-06-20 05:06:47', '', 0, 'http://localhost/maritime/?p=12', 2, 'nav_menu_item', '', 0),
(15, 1, '2015-06-19 22:17:43', '2015-06-19 15:17:43', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Product', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-product', '', '', '2015-06-29 21:17:16', '2015-06-29 14:17:16', '', 0, 'http://localhost/maritime/?post_type=product&#038;p=15', 0, 'product', '', 0),
(16, 1, '2015-06-19 22:17:36', '2015-06-19 15:17:36', '', 'Screen Shot 2015-06-19 at 22.17.30', '', 'inherit', 'open', 'open', '', 'screen-shot-2015-06-19-at-22-17-30', '', '', '2015-06-19 22:17:36', '2015-06-19 15:17:36', '', 15, 'http://localhost/maritime/wp-content/uploads/2015/06/Screen-Shot-2015-06-19-at-22.17.30.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2015-06-19 22:19:11', '2015-06-19 15:19:11', '', 'iridium-go-003', '', 'inherit', 'open', 'open', '', 'iridium-go-003', '', '', '2015-06-19 22:19:11', '2015-06-19 15:19:11', '', 15, 'http://localhost/maritime/wp-content/uploads/2015/06/iridium-go-003.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2015-06-19 22:20:23', '2015-06-19 15:20:23', '', 'Screen Shot 2015-06-19 at 22.20.16', '', 'inherit', 'open', 'open', '', 'screen-shot-2015-06-19-at-22-20-16', '', '', '2015-06-19 22:20:23', '2015-06-19 15:20:23', '', 15, 'http://localhost/maritime/wp-content/uploads/2015/06/Screen-Shot-2015-06-19-at-22.20.16.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2015-06-20 12:45:11', '2015-06-20 05:45:11', '', 'Shopping Cart', '', 'publish', 'open', 'open', '', 'shopping-cart', '', '', '2015-06-20 12:45:11', '2015-06-20 05:45:11', '', 0, 'http://localhost/maritime/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2015-06-20 13:49:46', '2015-06-20 06:49:46', '', 'Home', '', 'publish', 'open', 'open', '', 'home-2', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 0, 'http://localhost/maritime/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2015-06-29 11:35:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-29 11:35:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/maritime/?p=25', 0, 'post', '', 0),
(26, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '26', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 0, 'http://localhost/maritime/?p=26', 9, 'nav_menu_item', '', 0),
(27, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '27', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 16, 'http://localhost/maritime/?p=27', 10, 'nav_menu_item', '', 0),
(28, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '28', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 16, 'http://localhost/maritime/?p=28', 11, 'nav_menu_item', '', 0),
(29, 1, '2015-06-29 11:41:32', '2015-06-29 04:41:32', ' ', '', '', 'publish', 'open', 'open', '', '29', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 0, 'http://localhost/maritime/?p=29', 2, 'nav_menu_item', '', 0),
(30, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '30', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=30', 3, 'nav_menu_item', '', 0),
(31, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '31', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=31', 4, 'nav_menu_item', '', 0),
(32, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '32', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=32', 5, 'nav_menu_item', '', 0),
(33, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '33', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=33', 6, 'nav_menu_item', '', 0),
(34, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '34', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=34', 7, 'nav_menu_item', '', 0),
(35, 1, '2015-06-29 11:41:33', '2015-06-29 04:41:33', ' ', '', '', 'publish', 'open', 'open', '', '35', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 10, 'http://localhost/maritime/?p=35', 8, 'nav_menu_item', '', 0),
(36, 1, '2015-06-29 11:48:18', '2015-06-29 04:48:18', ' ', '', '', 'publish', 'open', 'open', '', '36', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 0, 'http://localhost/maritime/?p=36', 12, 'nav_menu_item', '', 0),
(38, 1, '2015-06-29 20:30:26', '2015-06-29 13:30:26', 'Lorem ipsum dolor sir amet', 'Location', '', 'publish', 'open', 'open', '', 'location', '', '', '2015-06-29 20:31:34', '2015-06-29 13:31:34', '', 0, 'http://localhost/maritime/?page_id=38', 0, 'page', '', 0),
(39, 1, '2015-06-29 20:30:26', '2015-06-29 13:30:26', 'Lorem ipsum dolor sir amet', 'Location', '', 'inherit', 'open', 'open', '', '38-revision-v1', '', '', '2015-06-29 20:30:26', '2015-06-29 13:30:26', '', 38, 'http://localhost/maritime/38-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2015-06-29 20:30:29', '2015-06-29 13:30:29', '<p>Lorem ipsum dolor sir amet</p>\n', 'Location', '', 'inherit', 'open', 'open', '', '38-autosave-v1', '', '', '2015-06-29 20:30:29', '2015-06-29 13:30:29', '', 38, 'http://localhost/maritime/38-autosave-v1/', 0, 'revision', '', 0),
(41, 1, '2015-06-29 20:30:46', '2015-06-29 13:30:46', ' ', '', '', 'publish', 'open', 'open', '', '41', '', '', '2015-06-29 20:30:46', '2015-06-29 13:30:46', '', 0, 'http://localhost/maritime/?p=41', 13, 'nav_menu_item', '', 0),
(42, 1, '2015-06-29 20:37:52', '2015-06-29 13:37:52', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Phone', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-phone', '', '', '2015-06-29 21:17:10', '2015-06-29 14:17:10', '', 0, 'http://localhost/maritime/produk/hello-phone/', 0, 'product', '', 0),
(43, 1, '2015-06-29 20:38:45', '2015-06-29 13:38:45', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Maritime', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-maritime', '', '', '2015-06-29 21:17:03', '2015-06-29 14:17:03', '', 0, 'http://localhost/maritime/produk/hello-maritime/', 0, 'product', '', 0),
(44, 1, '2015-06-29 20:39:10', '2015-06-29 13:39:10', '<p>Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span></p>\n', 'Hello Maritime', '<p>Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir</p>\n', 'inherit', 'open', 'open', '', '43-autosave-v1', '', '', '2015-06-29 20:39:10', '2015-06-29 13:39:10', '', 43, 'http://localhost/maritime/43-autosave-v1/', 0, 'revision', '', 0),
(45, 1, '2015-06-29 20:39:19', '2015-06-29 13:39:19', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Iridium', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-iridium', '', '', '2015-06-29 20:41:16', '2015-06-29 13:41:16', '', 0, 'http://localhost/maritime/produk/hello-iridium/', 0, 'product', '', 0),
(46, 1, '2015-06-29 20:39:42', '2015-06-29 13:39:42', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Thuraya', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-thuraya', '', '', '2015-06-29 20:40:55', '2015-06-29 13:40:55', '', 0, 'http://localhost/maritime/produk/hello-thuraya/', 0, 'product', '', 0),
(47, 1, '2015-06-29 20:40:03', '2015-06-29 13:40:03', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Hello Inmarsat', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'hello-inmarsat', '', '', '2015-06-29 20:40:40', '2015-06-29 13:40:40', '', 0, 'http://localhost/maritime/produk/hello-inmarsat/', 0, 'product', '', 0),
(48, 1, '2015-06-29 21:21:40', '2015-06-29 14:21:40', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Prepaid Hello', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'prepaid-hello', '', '', '2015-06-29 21:21:59', '2015-06-29 14:21:59', '', 0, 'http://localhost/maritime/produk/prepaid-hello/', 0, 'product', '', 0),
(49, 1, '2015-06-29 21:22:10', '2015-06-29 14:22:10', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Prepaid Goodbye', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'prepaid-goodbye', '', '', '2015-06-29 21:22:17', '2015-06-29 14:22:17', '', 0, 'http://localhost/maritime/produk/prepaid-goodbye/', 0, 'product', '', 0),
(50, 1, '2015-06-29 21:22:34', '2015-06-29 14:22:34', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ct ipsum dolor sir product <span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product </span><span style="line-height: 1.42857143;">ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product </span>', 'Prepaid Welcome', 'Lorem ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir produipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir product ipsum dolor sir', 'publish', 'open', 'closed', '', 'prepaid-welcome', '', '', '2015-06-29 21:22:44', '2015-06-29 14:22:44', '', 0, 'http://localhost/maritime/produk/prepaid-welcome/', 0, 'product', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_terms`
--

CREATE TABLE IF NOT EXISTS `mt_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_terms`
--

INSERT INTO `mt_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Tak Berkategori', 'tak-berkategori', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'Upper Right', 'upper-right', 0),
(7, 'Lower Left', 'lower-left', 0),
(8, 'Lower Right', 'lower-right', 0),
(9, 'Inmarsat Phone', 'inmarsat-phone', 0),
(10, 'Satellite Phones', 'satellite-phones', 0),
(11, 'Thuraya Phone', 'thuraya-phone', 0),
(12, 'Iridium Prepaid SIM', 'iridium-prepaid-sim', 0),
(13, 'Inmarsat Accecories', 'inmarsat-accecories', 0),
(14, 'Thuraya Accesories', 'thuraya-accesories', 0),
(15, 'Iridium Postpaid SIM', 'iridium-postpaid-sim', 0),
(16, 'Maritime and Mobile', 'maritime-and-mobile', 0),
(17, 'BGAN Hughes and Accesories', 'bgan-hughes-and-accesories', 0),
(18, 'Sailor Maritime Satellite Comunication', 'sailor-maritime-satellite-comunication', 0),
(19, 'Accecories', 'accecories', 0),
(20, 'booking', 'booking', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_term_relationships`
--

CREATE TABLE IF NOT EXISTS `mt_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_term_relationships`
--

INSERT INTO `mt_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(10, 6, 0),
(11, 6, 0),
(12, 6, 0),
(15, 2, 0),
(15, 19, 0),
(23, 8, 0),
(24, 7, 0),
(26, 7, 0),
(27, 7, 0),
(28, 7, 0),
(29, 7, 0),
(30, 7, 0),
(31, 7, 0),
(32, 7, 0),
(33, 7, 0),
(34, 7, 0),
(35, 7, 0),
(36, 7, 0),
(41, 7, 0),
(42, 2, 0),
(42, 19, 0),
(43, 2, 0),
(43, 19, 0),
(45, 2, 0),
(45, 9, 0),
(45, 10, 0),
(46, 2, 0),
(46, 10, 0),
(46, 11, 0),
(47, 2, 0),
(47, 9, 0),
(47, 10, 0),
(48, 2, 0),
(48, 12, 0),
(49, 2, 0),
(49, 12, 0),
(50, 2, 0),
(50, 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `mt_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_term_taxonomy`
--

INSERT INTO `mt_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 9),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'nav_menu', '', 0, 3),
(7, 7, 'nav_menu', '', 0, 13),
(8, 8, 'nav_menu', '', 0, 1),
(9, 9, 'product_cat', '', 10, 2),
(10, 10, 'product_cat', '', 0, 3),
(11, 11, 'product_cat', '', 10, 1),
(12, 12, 'product_cat', '', 10, 3),
(13, 13, 'product_cat', '', 10, 0),
(14, 14, 'product_cat', '', 10, 0),
(15, 15, 'product_cat', '', 10, 0),
(16, 16, 'product_cat', '', 0, 0),
(17, 17, 'product_cat', '', 16, 0),
(18, 18, 'product_cat', '', 16, 0),
(19, 19, 'product_cat', '', 0, 3),
(20, 20, 'product_type', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mt_usermeta`
--

CREATE TABLE IF NOT EXISTS `mt_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_usermeta`
--

INSERT INTO `mt_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'mtadmin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'mt_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'mt_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:1:{s:64:"62f8909a0798be1568aa11a4aa829d4da354f5be6ca13ccd37978bfaf8379597";a:4:{s:10:"expiration";i:1435775936;s:2:"ip";s:3:"::1";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36";s:5:"login";i:1434566336;}}'),
(15, 1, 'mt_dashboard_quick_press_last_post_id', '25'),
(16, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(17, 1, 'metaboxhidden_dashboard', 'a:2:{i:0;s:36:"woocommerce_dashboard_recent_reviews";i:1;s:17:"dashboard_primary";}'),
(18, 1, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:78:"dashboard_right_now,dashboard_quick_press,woocommerce_dashboard_recent_reviews";s:4:"side";s:36:"dashboard_activity,dashboard_primary";s:7:"column3";s:28:"woocommerce_dashboard_status";s:7:"column4";s:0:"";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:10:{i:0;s:8:"add-post";i:1;s:11:"add-product";i:2;s:12:"add-resource";i:3;s:13:"add-apf_posts";i:4;s:12:"add-post_tag";i:5;s:15:"add-post_format";i:6;s:15:"add-product_tag";i:7;s:21:"add-resource_category";i:8;s:23:"add-apf_sample_taxonomy";i:9;s:30:"woocommerce_endpoints_nav_link";}'),
(21, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:2:{s:32:"9bf31c7ff062936a96d3c8bd1f8f2ff3";a:9:{s:10:"product_id";i:15;s:12:"variation_id";s:0:"";s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:40000;s:8:"line_tax";i:0;s:13:"line_subtotal";i:40000;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}s:32:"d9d4f495e875a2e075a1a4a6e1b9770f";a:9:{s:10:"product_id";i:46;s:12:"variation_id";s:0:"";s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:20000;s:8:"line_tax";i:0;s:13:"line_subtotal";i:20000;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}}'),
(22, 1, 'mt_user-settings', 'libraryContent=browse&posts_list_mode=list'),
(23, 1, 'mt_user-settings-time', '1435585606'),
(24, 1, 'nav_menu_recently_edited', '7'),
(25, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(26, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(27, 1, 'meta-box-order_product', 'a:3:{s:4:"side";s:84:"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images";s:6:"normal";s:67:"woocommerce-product-data,postcustom,slugdiv,postexcerpt,commentsdiv";s:8:"advanced";s:0:"";}'),
(28, 1, 'screen_layout_product', '2');

-- --------------------------------------------------------

--
-- Table structure for table `mt_users`
--

CREATE TABLE IF NOT EXISTS `mt_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_users`
--

INSERT INTO `mt_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'mtadmin', '$P$Bp4xly7uGCxKNPmeB4wtzuObUUOSU7/', 'mtadmin', 'hi@galihpratama.net', '', '2015-06-17 18:37:31', '', 0, 'mtadmin');

-- --------------------------------------------------------

--
-- Table structure for table `mt_wc_booking_relationships`
--

CREATE TABLE IF NOT EXISTS `mt_wc_booking_relationships` (
  `ID` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `resource_id` bigint(20) unsigned NOT NULL,
  `sort_order` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_attribute_taxonomies`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL,
  `attribute_name` varchar(200) NOT NULL,
  `attribute_label` longtext,
  `attribute_type` varchar(200) NOT NULL,
  `attribute_orderby` varchar(200) NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_downloadable_product_permissions`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL,
  `download_id` varchar(32) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_order_itemmeta`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_order_items`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL,
  `order_item_name` longtext NOT NULL,
  `order_item_type` varchar(200) NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_tax_rates`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL,
  `tax_rate_country` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) NOT NULL DEFAULT '',
  `tax_rate` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_tax_rate_locations`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL,
  `location_code` varchar(255) NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mt_woocommerce_termmeta`
--

CREATE TABLE IF NOT EXISTS `mt_woocommerce_termmeta` (
  `meta_id` bigint(20) NOT NULL,
  `woocommerce_term_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mt_woocommerce_termmeta`
--

INSERT INTO `mt_woocommerce_termmeta` (`meta_id`, `woocommerce_term_id`, `meta_key`, `meta_value`) VALUES
(1, 9, 'order', '0'),
(2, 9, 'display_type', ''),
(3, 9, 'thumbnail_id', '0'),
(4, 10, 'order', '0'),
(5, 10, 'display_type', ''),
(6, 10, 'thumbnail_id', '0'),
(7, 11, 'order', '0'),
(8, 11, 'display_type', ''),
(9, 11, 'thumbnail_id', '0'),
(10, 12, 'order', '0'),
(11, 12, 'display_type', ''),
(12, 12, 'thumbnail_id', '0'),
(13, 13, 'order', '0'),
(14, 13, 'display_type', ''),
(15, 13, 'thumbnail_id', '0'),
(16, 14, 'order', '0'),
(17, 14, 'display_type', ''),
(18, 14, 'thumbnail_id', '0'),
(19, 15, 'order', '0'),
(20, 15, 'display_type', ''),
(21, 15, 'thumbnail_id', '0'),
(22, 16, 'order', '0'),
(23, 16, 'display_type', ''),
(24, 16, 'thumbnail_id', '0'),
(25, 17, 'order', '0'),
(26, 17, 'display_type', ''),
(27, 17, 'thumbnail_id', '0'),
(28, 18, 'order', '0'),
(29, 18, 'display_type', ''),
(30, 18, 'thumbnail_id', '0'),
(31, 19, 'order', '0'),
(32, 19, 'display_type', ''),
(33, 19, 'thumbnail_id', '0'),
(34, 10, 'product_count_product_cat', '6'),
(35, 9, 'product_count_product_cat', '2'),
(36, 11, 'product_count_product_cat', '1'),
(37, 12, 'product_count_product_cat', '3'),
(38, 16, 'product_count_product_cat', '0'),
(39, 19, 'product_count_product_cat', '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mt_booking_block_price_attribute_meta`
--
ALTER TABLE `mt_booking_block_price_attribute_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_booking_block_price_meta`
--
ALTER TABLE `mt_booking_block_price_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_booking_fixed_blocks`
--
ALTER TABLE `mt_booking_fixed_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_booking_history`
--
ALTER TABLE `mt_booking_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_booking_order_history`
--
ALTER TABLE `mt_booking_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mt_commentmeta`
--
ALTER TABLE `mt_commentmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `comment_id` (`comment_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `mt_comments`
--
ALTER TABLE `mt_comments`
  ADD PRIMARY KEY (`comment_ID`), ADD KEY `comment_post_ID` (`comment_post_ID`), ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`), ADD KEY `comment_date_gmt` (`comment_date_gmt`), ADD KEY `comment_parent` (`comment_parent`), ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `mt_links`
--
ALTER TABLE `mt_links`
  ADD PRIMARY KEY (`link_id`), ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `mt_options`
--
ALTER TABLE `mt_options`
  ADD PRIMARY KEY (`option_id`), ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `mt_postmeta`
--
ALTER TABLE `mt_postmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `post_id` (`post_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `mt_posts`
--
ALTER TABLE `mt_posts`
  ADD PRIMARY KEY (`ID`), ADD KEY `post_name` (`post_name`(191)), ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`), ADD KEY `post_parent` (`post_parent`), ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `mt_terms`
--
ALTER TABLE `mt_terms`
  ADD PRIMARY KEY (`term_id`), ADD KEY `slug` (`slug`(191)), ADD KEY `name` (`name`(191));

--
-- Indexes for table `mt_term_relationships`
--
ALTER TABLE `mt_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`), ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `mt_term_taxonomy`
--
ALTER TABLE `mt_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`), ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`), ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `mt_usermeta`
--
ALTER TABLE `mt_usermeta`
  ADD PRIMARY KEY (`umeta_id`), ADD KEY `user_id` (`user_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `mt_users`
--
ALTER TABLE `mt_users`
  ADD PRIMARY KEY (`ID`), ADD KEY `user_login_key` (`user_login`), ADD KEY `user_nicename` (`user_nicename`);

--
-- Indexes for table `mt_wc_booking_relationships`
--
ALTER TABLE `mt_wc_booking_relationships`
  ADD PRIMARY KEY (`ID`), ADD KEY `product_id` (`product_id`), ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `mt_woocommerce_attribute_taxonomies`
--
ALTER TABLE `mt_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`), ADD KEY `attribute_name` (`attribute_name`(191));

--
-- Indexes for table `mt_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `mt_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`), ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`), ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`);

--
-- Indexes for table `mt_woocommerce_order_itemmeta`
--
ALTER TABLE `mt_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `order_item_id` (`order_item_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `mt_woocommerce_order_items`
--
ALTER TABLE `mt_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`), ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `mt_woocommerce_tax_rates`
--
ALTER TABLE `mt_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`), ADD KEY `tax_rate_country` (`tax_rate_country`(191)), ADD KEY `tax_rate_state` (`tax_rate_state`(191)), ADD KEY `tax_rate_class` (`tax_rate_class`(191)), ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `mt_woocommerce_tax_rate_locations`
--
ALTER TABLE `mt_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`), ADD KEY `tax_rate_id` (`tax_rate_id`), ADD KEY `location_type` (`location_type`), ADD KEY `location_type_code` (`location_type`,`location_code`(191));

--
-- Indexes for table `mt_woocommerce_termmeta`
--
ALTER TABLE `mt_woocommerce_termmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `woocommerce_term_id` (`woocommerce_term_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mt_booking_block_price_attribute_meta`
--
ALTER TABLE `mt_booking_block_price_attribute_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_booking_block_price_meta`
--
ALTER TABLE `mt_booking_block_price_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_booking_fixed_blocks`
--
ALTER TABLE `mt_booking_fixed_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_booking_history`
--
ALTER TABLE `mt_booking_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_booking_order_history`
--
ALTER TABLE `mt_booking_order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_commentmeta`
--
ALTER TABLE `mt_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_comments`
--
ALTER TABLE `mt_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_links`
--
ALTER TABLE `mt_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_options`
--
ALTER TABLE `mt_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=589;
--
-- AUTO_INCREMENT for table `mt_postmeta`
--
ALTER TABLE `mt_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=515;
--
-- AUTO_INCREMENT for table `mt_posts`
--
ALTER TABLE `mt_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `mt_terms`
--
ALTER TABLE `mt_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mt_term_taxonomy`
--
ALTER TABLE `mt_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mt_usermeta`
--
ALTER TABLE `mt_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `mt_users`
--
ALTER TABLE `mt_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mt_wc_booking_relationships`
--
ALTER TABLE `mt_wc_booking_relationships`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_attribute_taxonomies`
--
ALTER TABLE `mt_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `mt_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_order_itemmeta`
--
ALTER TABLE `mt_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_order_items`
--
ALTER TABLE `mt_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_tax_rates`
--
ALTER TABLE `mt_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_tax_rate_locations`
--
ALTER TABLE `mt_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mt_woocommerce_termmeta`
--
ALTER TABLE `mt_woocommerce_termmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
